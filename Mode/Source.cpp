/*
Mode - The mode of a set of data values is the value that appears most often.
References
[1]C++ Encontrar la Moda en un Arreglo, youtube, https://www.youtube.com/watch?v=hnZQ5UKh6rQ
*/
#include<iostream>
using namespace std;
int Check(double data[], double number, int length)
{
	int x = 0;
	for (int i = 0; i <= length; i++)
	{
		if (number == data[i])
		{
			x++;
		}
	}
	return x;
}
double mode(double data[],int length)
{
	double number = 0.0;
	double value = 0.0;
	int repetition = 0;
	for (int i = 0; i <= length; i++)
	{
		number = data[i];
		if (repetition<= Check(data,number,length))
		{
			repetition = Check(data, number, length);
			value = data[i];
		}
	}
	return value;
}
int main()
{
	double CDAM21[8] = { 1.02, 0.99, 0.84, 1.06, 1.02, 1.02, 0.99, 0.99 };
	cout << "Mode : " << mode(CDAM21, 8) << endl;

	double CDAM11[24] = { 1.49, 1.09, 0.96, 1.22, 1.24, 1.32, 1.21, 0.99, 0.96, 1.06, 1.02, 1.05,
		1.02, 0.96, 1.18, 0.99, 0.98, 1.86, 0.99, 1.15, 1.21, 1.08, 1.06, 0.96 };
	cout << "Mode : " << mode(CDAM11, 24) << endl;

	return 0;
}